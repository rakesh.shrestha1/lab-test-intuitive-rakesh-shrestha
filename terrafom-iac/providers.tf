terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }

  backend "s3" {
    bucket  = "lab-test-intuitive-s3" # bucket should be initialized before initializing the terraform code
    key     = "lab-test-intuitive-s3-state"
    region = "us-east-1"
    encrypt = true
    skip_credentials_validation = true
  }
}

provider "aws" {
  region     = var.AWS_REGION
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

