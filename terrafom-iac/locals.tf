locals {
  project             = "lab-test-intuitive"
  project_description = "lab-test-intuitive"
  environment         = "lab-test"
  owner               = "rakesh.shrestha"
  maintainer          = "rakesh.shrestha"
}

locals {
  vpc_cidr = "10.123.0.0/16"
}

locals {
  security_groups = {
    public = {
      name        = "public_sg"
      description = "Security group for Public Access"
      ingress = {
        ssh = {
          from        = 22
          to          = 22
          protocol    = "tcp"
          cidr_blocks = ["103.10.28.134/32"] //rakesh-shrestha-public-ip-home
        }
      }
    }
  }
}
