module "vpc" {
  source          = "./vpc"
  vpc_cidr        = local.vpc_cidr
  security_groups = local.security_groups
}

module "ec2" {
  source        = "./ec2"
  public_sg     = module.vpc.public_sg
  public_subnet = module.vpc.public_subnet
}